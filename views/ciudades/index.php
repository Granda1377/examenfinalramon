<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ciudades';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ciudades-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Ciudades', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'habitantes',
            //'escudo',
            [
            
            'label'=>'Escudo',
            'format'=>'raw',
            'content'=>function($modelo){
                 
                 if (
                         $modelo->escudo == "" 
                 ){
                     return Html::img("@web/imgs/" . "escudo.jpg", ['width' => '100px']);
                 }
                 else {
                     return Html::img("@web/imgs/" . $modelo->escudo,['width' => '100px']);
                 }
            }
            ],
            //'mapa',
            [
            
            'label'=>'Mapa',
            'format'=>'raw',
            'content'=>function($modelo){
                
                if (
                         $modelo->mapa == "" 
                 ){
                     return Html::img("@web/imgs/" . "mapa.png", ['width' => '100px']);
                 }
                 else {
                     return Html::img("@web/imgs/" . $modelo->mapa,['width' => '100px']);
                 }
                  
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
